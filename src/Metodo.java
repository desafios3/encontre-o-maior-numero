import java.util.ArrayList;
import java.util.Collections;

public class Metodo {

    ArrayList<Valor> lista = new ArrayList<Valor>();

    public void cadastrar(int valor) {
        Valor valores = new Valor(valor);
        lista.add(valores);
    }

    public void ordenaValores() {
        Collections.sort(lista, new Comparador());
    }

}
