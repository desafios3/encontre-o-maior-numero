import java.util.Comparator;

public class Comparador implements Comparator<Valor>{

    @Override
    public int compare(Valor v1, Valor v2) {
        return v1.getValor() - v2.getValor();
    }

}